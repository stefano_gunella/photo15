package com.gun;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class GameOf15 extends Activity{
	final String TAG = "GameOf15";
	final boolean _DEBUG_ = false;
	//	Per aggiungere pubblicit� andare su:
	//	https://developers.google.com/mobile-ads-sdk/docs/admob/android/quick-start
	//	https://apps.admob.com/#home

	public GameView gameView = null;
	public Chronometer chrono = null;
	public boolean start_to_solve = false;
	public boolean solved = false;
	Animation pulse_animation = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//		requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
		setContentView(R.layout.game15);
		gameView = (GameView)this.findViewById(R.id.desktop);
		chrono = (Chronometer)findViewById(R.id.chronometer);
		pulse_animation = AnimationUtils.loadAnimation(this, R.anim.pulse_animation);
		initializeButtons();
		loadLastImage();

		// Look up the AdView as a resource and load a request.
		AdView adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder()
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
		.addTestDevice("F45EC21C8718E10D61507DB01DBA4A63")
		.build();	// il TESTDEVICE lo si legge dal LOG

		adView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		adView.loadAd(adRequest);
	}

	@Override
	public void onStart() {
		super.onStart();
		final SharedPreferences settings = getSharedPreferences("localPreferences", MODE_PRIVATE);
		if (settings.getBoolean("isFirstRun", true)||_DEBUG_) {
			new AlertDialog.Builder(this)
			.setTitle("Cookies")
			.setMessage(this.getString(R.string.MESSAGE_COOKIES))
			.setPositiveButton(this.getString(R.string.MESSAGE_SHOW_COOKIES_DETAIL), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which){
					try {
						Uri uri = Uri.parse("http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm");
						Intent intent = new Intent(Intent.ACTION_VIEW,uri);
						startActivity(intent);
					}
					catch (ActivityNotFoundException e) {
						Toast.makeText(GameOf15.this, "Application not found", Toast.LENGTH_SHORT).show();
					}
				}
			})
			.setNegativeButton(this.getString(R.string.MESSAGE_HIDE_COOKIES_DETAIL), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which){
					settings.edit().putBoolean("isFirstRun", false).commit();
				}
			})
			.setNeutralButton(this.getString(R.string.MESSAGE_KISSENE), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which){
					settings.edit().putBoolean("isFirstRun", false).commit();
				}
			}).show();
		}
	}

	public void initializeButtons(){	
		runOnUiThread(new Runnable(){
			public void run(){
				start_to_solve = false;
				findViewById(R.id.button_shake).startAnimation(pulse_animation);				
			}
		});
	}

	//	@Override
	//	public boolean onCreateOptionsMenu(Menu menu) {
	//		super.onCreateOptionsMenu(menu);
	//		menu.add(0, MENU_START, 0, R.string.menu_start);
	//		menu.add(0, MENU_STOP, 0, R.string.menu_stop);
	//		return true;
	//	}

	//	private int mActivePointerId;

	//	public boolean onTouchEvent(MotionEvent event) {
	//		mActivePointerId = event.getPointerId(0);
	//		Log.d(TAG, "**** MULTI TOUCH ****"+event.getPointerCount());
	//
	//		// Use the pointer ID to find the index of the active pointer 
	//		// and fetch its position
	//		//		int pointerIndex = event.findPointerIndex(mActivePointerId);
	//		// Get the pointer's current position
	//		//		float x = event.getX(pointerIndex);
	//		//		float y = event.getY(pointerIndex);
	//		return true;
	//	}

	/**********************************************/
	public void reset(View v) {
		synchronized (this) {
			Log.d(TAG, "reset");
			findViewById(R.id.button_reset).setEnabled(false);
			findViewById(R.id.button_shake).setEnabled(false);
			gameView.initGameTilePosition(true);
			gameView.playSoudEffect(3);
			findViewById(R.id.button_reset).setEnabled(true);
			findViewById(R.id.button_shake).setEnabled(true);
			resetChronometer();
			resetCounter();
			start_to_solve = false;
			solved = false;
			findViewById(R.id.button_shake).startAnimation(pulse_animation);
		}
	}

	static final int PICK_FROM_GALLERY = 100;
	public Uri selectedImageUri = null;

	public void cropImageFromGallery(View v) {
		synchronized (this) {
			Log.d(TAG, "cropImageFromGallery SDK VERSION="+Build.VERSION.SDK_INT);
			Intent intent = null;
			File tempDir= Environment.getExternalStorageDirectory();
			File path = new File (tempDir.getAbsolutePath()+"/gameOf15");
			if (!path.exists()){
				path.mkdir();
			}
			File tempFile = new File (path,"output.jpg");
			try {
				if(isSomeGalleryInstalled(this)){
					intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_PICK);
					intent.putExtra("crop", "true");
					intent.putExtra("aspectX", 1);
					intent.putExtra("aspectY", 1);
					intent.putExtra("outputX", 400);
					intent.putExtra("outputY", 400);
					intent.putExtra("scale", true);
					intent.putExtra("return-data", false);
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile)); // indica all'intent dove salvare l'immagine ritagliata
					startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_FROM_GALLERY);
				}
				else{
					AlertDialog.Builder alert = new AlertDialog.Builder(this);
					alert.setTitle(this.getString(R.string.MESSAGE_WARNING));
					alert.setMessage(this.getString(R.string.MESSAGE_NO_GALLERY_APPLICATION_INSTALLED));
					alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							dialog.cancel();
						}
					});
					alert.show();
				}
			}
			catch (ActivityNotFoundException anfe) {
				intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile)); // indica all'intent dove salvare l'immagine ritagliata
				startActivityForResult(intent, PICK_FROM_GALLERY);
			}
		}
	}

	/*
	public static boolean isPackageInstalled(String packagename, Context context) {
	    PackageManager pm = context.getPackageManager();
	    try {
	        pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
	        return true;
	    } catch (NameNotFoundException e) {
	        return false;
	    }
	}
	 */

	//	private Image getImageFromCloud(){
	//		BlobKey blobKey;  // ...
	//		ImagesService imagesService = ImagesServiceFactory.getImagesService();
	//		Image oldImage = ImagesServiceFactory.makeImageFromBlob(blobKey);
	//		Transform resize = ImagesServiceFactory.makeResize(200, 300);
	//		Image newImage = imagesService.applyTransform(resize, oldImage);
	//		byte[] newImageData = newImage.getImageData();
	//	}

	public boolean isSomeGalleryInstalled(Context context) {
		PackageManager pm = context.getPackageManager();
		final Intent i = new Intent(Intent.ACTION_VIEW, null);
		i.setType("image/*");
		final List<ResolveInfo> apps = pm.queryIntentActivities(i, 0);
		if(apps != null) {
			for(ResolveInfo info : apps) {
				String name = info.activityInfo.name;
				if(name!=null) {
					//This is the target you want
					//startActivity(XXXX);
					Log.d(TAG, "-----package:"+name);
				}
			}
			return true;
		} 
		return false;
	}

	/***
	 * ATTENZIONE:nel manifest l'applicazione deve essere diversa da SingleInstance!!!
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(TAG, "onActivityResult");
		if (requestCode == PICK_FROM_GALLERY && null==data && resultCode==-1) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle(this.getString(R.string.MESSAGE_WARNING));
			alert.setMessage(this.getString(R.string.MESSAGE_NO_GALLERY_APPLICATION_INSTALLED));
			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.cancel();
				}
			});
			alert.show();
			loadLastImage();
		}
		else if (requestCode == PICK_FROM_GALLERY && null==data && resultCode==0) {
			// do nothing...no image selected
		}
		else if (requestCode == PICK_FROM_GALLERY && null!=data.getData()) {
			Uri selectedImageUri = data.getData();
			String selectedImagePath = _getPath(selectedImageUri);
			Bitmap photo = BitmapFactory.decodeFile(selectedImagePath);
			Log.d(TAG, "selectedImagePath >>> "+selectedImagePath);
			Log.d(TAG, "photo >>> "+photo);
			if (selectedImagePath != null && null!=photo && resultCode==-1) {
				String absPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/gameOf15/output.jpg";
				FileOutputStream fOut;
				try {
					fOut = new FileOutputStream(new File(absPath));
					photo.compress(Bitmap.CompressFormat.PNG, 100, fOut);
					fOut.flush();
					fOut.close();
				} 
				catch (FileNotFoundException e) {
					e.printStackTrace();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
				loadLastImage();
			}
			else{
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.setTitle(this.getString(R.string.MESSAGE_WARNING));
				alert.setMessage(this.getString(R.string.MESSAGE_NO_IMAGE_FOUND));
				alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.cancel();
					}
				});
				alert.show();
			}
		}
	}

	public void loadLastImage() {
		Log.d(TAG, "loadLastImage");
		Handler handler = new Handler(); 
		handler.postDelayed(new Runnable() { 
			public void run() { 
				InputStream is = null;
				try {
					String absPath= Environment.getExternalStorageDirectory().getAbsolutePath();
					File path = new File (absPath+"/gameOf15/output.jpg");
					Bitmap b = null;
					Log.d(TAG, "PATH:"+path.toString());
					if (path.exists()){
						is = new FileInputStream(path);
						b = BitmapFactory.decodeStream(is);
						is.close();
					}
					gameView.setBitmapBackground(b);
					gameView.calcGameTilePosition();			
				} 
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				catch (Exception e) {
					Log.d(TAG, "___ERRORE "+e.getMessage());
				}
			} 
		}, 100);
	}

	public void shake(View v) {
		synchronized (this) {
			Log.d(TAG, "mix");
			if(solved){
				resetChronometer();
				resetCounter();
				solved = false;
			}
			findViewById(R.id.button_reset).setEnabled(false);
			findViewById(R.id.button_shake).setEnabled(false);
			gameView.shake();
			start_to_solve = true;
			findViewById(R.id.button_shake).clearAnimation();
		}
	}

	boolean startedChrono = false;
	public void startChronometer() {
		if(startedChrono){
			return;
		}
		synchronized (this) {
			startedChrono = true;
			Log.d(TAG, "startChronometer");
			chrono.setBase(SystemClock.elapsedRealtime());
			chrono.start();
		}
	}

	public void resetChronometer() {
		startedChrono = false;
		chrono.setBase(SystemClock.elapsedRealtime());
		chrono.stop();
	}

	public void stopChronometer() {
		if(!startedChrono) 
			return;
		synchronized (this) {
			startedChrono = false;
			Log.d(TAG, "stopChronometer");
			chrono.stop();
		}
	}

	int moveCounter = 0;
	public void increaseCounter() {
		synchronized (this) {
			if(!gameView.isShaking){
				runOnUiThread(new Runnable(){
					public void run(){
						moveCounter++;
						Log.d(TAG, "increaseCounter");
						((TextView)findViewById(R.id.label_move_counter)).setText("MOVE#"+moveCounter);						
					}
				});
			}
		}
	}

	public void resetCounter() {
		moveCounter = 0;
		((TextView)findViewById(R.id.label_move_counter)).setText("MOVE#");
	}

	public String _getPath(Uri uri) {
		String result = null;
		// just some safety built in 
		if( uri == null ) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] data_columm = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, data_columm, null, null, null);
		if( cursor != null ){
			cursor.moveToFirst();
			int column_index = cursor.getColumnIndexOrThrow(data_columm[0]);
			result = cursor.getString(column_index);
		}
		else{
			result = uri.getPath();
		}
		return result;
	}

	//    @Override
	//    protected void onSaveInstanceState(Bundle outState) {
	//        // just have the View's thread save its state into our Bundle
	//        super.onSaveInstanceState(outState);
	//        outState.putBoolean("init_game_tile", init_game_tile);
	//        Log.w(this.getClass().getName(), "SIS called");
	//    }

	//    @Override
	//    protected void onRestoreInstanceState(Bundle outState) {
	//        // just have the View's thread save its state into our Bundle
	//        super.onSaveInstanceState(outState);
	//        init_game_tile = outState.getBoolean("init_game_tile");
	//        Log.w(this.getClass().getName(), "SIS called");
	//    }
}