package com.gun;

import java.util.ArrayList;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import static com.gun.GameParameters.*;

public class GameView extends SurfaceView implements SurfaceHolder.Callback{
	final String TAG = "GameView";
	private SurfaceHolder holder; // contenitore della view
	Tile[][] tiles = null;
	SoundPool sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
	boolean isShaking = false;
	boolean isMoving = false;
	GameOf15 mainActivity = null;
	int soundIds[] = new int[4];

	public GameView(Context context,AttributeSet attrs) {
		super(context, attrs);
		holder = getHolder();
		holder.addCallback(this);
		soundIds[0] = sp.load(getContext(), R.raw.slide_tile1, 1);
		soundIds[1] = sp.load(getContext(), R.raw.slide_tile2, 1);
		soundIds[2] = sp.load(getContext(), R.raw.applause, 1);
		soundIds[3] = sp.load(getContext(), R.raw.reset_tiles, 1);
		mainActivity = (GameOf15)GameView.this.getContext();
	}

	protected void playSoudEffect(int index){
		sp.play(soundIds[index], 1, 1, 0, 0, 1.0f);	
	}

	@SuppressLint("WrongCall")
	public void initGameTilePosition(boolean isreset) {
		Log.d(TAG, "initGameTilePosition"); 
		if(tiles==null || isreset){
			tiles = new Tile[GameParameters.NUM_TILES][GameParameters.NUM_TILES];
			GameParameters.VIEW_WIDTH  = holder.getSurfaceFrame().right;
			GameParameters.VIEW_HEIGHT = holder.getSurfaceFrame().bottom;
			int count = 1;
			for (int i = 0; i < GameParameters.NUM_TILES; i++) {
				for (int j = 0; j < GameParameters.NUM_TILES; j++) {	
					tiles[j][i] = new Tile(""+count++,j,i,this);
				}
			}
			tiles[GameParameters.NUM_TILES-1][GameParameters.NUM_TILES-1] = null;
		}
		mainActivity.loadLastImage();
		//		Canvas c = holder.lockCanvas();
		//		onDraw(c);
		//		holder.unlockCanvasAndPost(c);
	}

	@SuppressLint("WrongCall")
	public void calcGameTilePosition() {
		GameParameters.VIEW_WIDTH  = holder.getSurfaceFrame().right;
		GameParameters.VIEW_HEIGHT = holder.getSurfaceFrame().bottom;
		for (int i = 0; i < GameParameters.NUM_TILES; i++) {
			for (int j = 0; j < GameParameters.NUM_TILES; j++) {
				if(tiles[j][i]!=null){
					tiles[j][i].initPosition(this);
				}
			}
		}
		Canvas c = holder.lockCanvas();
		onDraw(c);
		holder.unlockCanvasAndPost(c);
	}

	private Bitmap bitmapBackground = null;
	Paint p= new Paint();

	@Override
	protected void onDraw(Canvas canvas) {
		//		Bitmap myicon=BitmapFactory.decodeResource(getResources(),R.drawable.icon);
		if(canvas!=null){
			canvas.drawColor(Color.BLACK);
			//			canvas.drawBitmap(myicon, 0, 0, p);
			//			if(null!=bitmapBackground){
			//				canvas.drawBitmap(bitmapBackground, 0, 0, null);
			//			}

			//		canvas.drawColor(Color.argb(0, 255, 255, 255));
			for (int i = 0; i < GameParameters.NUM_TILES; i++) {
				for (int j = 0; j < GameParameters.NUM_TILES; j++) {
					if(tiles[j][i]!=null)
						tiles[j][i].drawOn(canvas);
				}
			}
		}
	}

	Tile touchTile = null;
	MoveTileThread thread = null;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (isShaking || isMoving || !mainActivity.start_to_solve){
			return false;
		}
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			Log.d(TAG, "ACTION_DOWN"); 
			touchTile = getTouchedTile(event.getX(),event.getY());
			if(touchTile!=null){
				mainActivity.startChronometer();
				thread = new MoveTileThread(touchTile);
				if(!thread.isAlive()){
					thread.start();
				}
			}
			break;
		case MotionEvent.ACTION_UP:
			Log.d(TAG, "ACTION_UP");
			if(touchTile!=null){
				touchTile.updateCurrentPos();
				touchTile = null;
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			Log.d(TAG, "ACTION_CANCEL");
			break;
		case MotionEvent.ACTION_OUTSIDE:
			Log.d(TAG, "ACTION_OUTSIDE");
			break;
		default:
		}
		return true;
	}

	private Tile getTouchedTile(float x,float y) {
		for (int i = 0; i < GameParameters.NUM_TILES; i++) {
			for (int j = 0; j < GameParameters.NUM_TILES; j++) {
				if(tiles[j][i] != null && tiles[j][i].isTouched(x, y))
					return tiles[j][i];
			}
		}
		return null;
	}

	private PositionEmptyTile getEmtyPosition() {
		for (int i = 0; i < GameParameters.NUM_TILES; i++) {
			for (int j = 0; j < GameParameters.NUM_TILES; j++) {
				if(tiles[j][i] == null)
					return new PositionEmptyTile(j,i);
			}
		}
		return null;
	}

	private ArrayList<Tile> getAllTilesAtRow(int row) {
		ArrayList<Tile> lresult = new ArrayList<Tile>();
		for (int i = 0; i < GameParameters.NUM_TILES; i++) {
			if(tiles[row][i] != null){
				lresult.add(tiles[row][i]);
			}
		}
		return lresult;
	}

	private ArrayList<Tile> getAllTilesAtColumn(int col) {
		ArrayList<Tile> lresult = new ArrayList<Tile>();
		for (int i = 0; i < GameParameters.NUM_TILES; i++) {
			if(tiles[i][col] != null){
				lresult.add(tiles[i][col]);
			}
		}
		return lresult;
	}

	private ArrayList<Tile> getAllTilesToMoveH(Tile t,PositionEmptyTile e) {
		ArrayList<Tile> lresult = new ArrayList<Tile>();
		if(t.posX<e.x){
			for (int i = t.posX; i <= e.x; i++) {
				if(tiles[i][e.y] != null){
					lresult.add(tiles[i][e.y]);
				}
			}
		}
		else{
			for (int i = t.posX; i >= e.x; i--) {
				if(tiles[i][e.y] != null){
					lresult.add(tiles[i][e.y]);
				}
			}
		}
		return lresult;
	}

	private ArrayList<Tile> getAllTilesToMoveV(Tile t,PositionEmptyTile e) {
		ArrayList<Tile> lresult = new ArrayList<Tile>();
		if(t.posY<e.y){
			for (int i = t.posY; i <= e.y; i++) {
				if(tiles[e.x][i] != null){
					lresult.add(tiles[e.x][i]);
				}
			}
		}
		else{
			for (int i = t.posY; i >= e.y; i--) {
				if(tiles[e.x][i] != null){
					lresult.add(tiles[e.x][i]);
				}
			}
		}
		return lresult;
	}

	class PositionEmptyTile{
		public int x;
		public int y;
		public PositionEmptyTile(int x,int y){
			this.x = x;
			this.y = y;			
		}
	}

	/**************************************/
	public void shake() {
		ShakeTileThread st = new ShakeTileThread();
		st.start();
	}

	class ShakeTileThread extends Thread {
		
		MoveTileThread mth = null;
		@Override
		public void run() {
			Log.d(TAG, "start ShakeTileThread");
			isShaking = true;
			Tile tile = null;
			Random rnd = new Random();
			for (int i = 0; i < GameParameters.NUM_SHAKE_MOVE; i++) {
				Log.d(TAG, "move #"+i);
				PositionEmptyTile pEmpty = getEmtyPosition();
				int rIndex = 0;

				if(i%2==0){
					lTiles = getAllTilesAtRow(pEmpty.x);
					rIndex = rnd.nextInt(3);
					tile = lTiles.get(rIndex);
				}
				else{
					lTiles = getAllTilesAtColumn(pEmpty.y);
					rIndex = rnd.nextInt(3);
					tile = lTiles.get(rIndex);	
				}
				mth = new MoveTileThread(tile);
				mth.start();
				try {
					mth.join();
					Thread.sleep(100);
				}
				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}

			mainActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mainActivity.findViewById(R.id.button_reset).setEnabled(true);
					mainActivity.findViewById(R.id.button_shake).setEnabled(true);
				}
			});
			isShaking = false;
		}
	}

	/**************************************/
	ArrayList<Tile> lTiles = null;
	class MoveTileThread extends Thread {
		Tile tile = null;

		public MoveTileThread(Tile t){
			tile = t;
		}

		@Override
		public void run() {
			if(tile==null){
				return;
			}
			isMoving = true;
			PositionEmptyTile pEmpty = getEmtyPosition();
			if(tile.posX == pEmpty.x){
				mainActivity.increaseCounter();
				translateAllTilesVertical(tile,pEmpty);
			}
			else if(tile.posY == pEmpty.y){
				mainActivity.increaseCounter();
				translateAllTilesHorizontal(tile,pEmpty);
			}
			isMoving = false;
			if (checkSolution()){
				mainActivity.stopChronometer();
				mainActivity.solved = true;
				playSoudEffect(2);
				long[] pattern = {0, 500, 1000, 500, 1000, 500};
				Vibrator v = (Vibrator) mainActivity.getSystemService(Context.VIBRATOR_SERVICE);
				v.vibrate(pattern,-1);
				mainActivity.initializeButtons();
			}
		}
	}

	private void dumpTiles() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < GameParameters.NUM_TILES; i++) {
			for (int j = 0; j < GameParameters.NUM_TILES; j++) {
				if(tiles[j][i]!=null)
					sb.append("-"+tiles[j][i].label+"-");
				else
					sb.append("-NULL-");
			}
			sb.append("\n");
		}
		Log.d(TAG,sb.toString());
	}

	//	Handler gameHandler = new Handler() {
	//		@Override
	//		public void handleMessage(Message m) {
	//		}
	//	};

	/**********************************************/
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "surfaceDestroyed");
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated");
		initGameTilePosition(false);

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		//		Log.d(TAG, "surfaceChanged:"+bitmapBackground);
		//		calcGameTilePosition();
	}
	/**********************************************/
	@SuppressLint("WrongCall")
	private void translateAllTilesHorizontal(Tile tile, PositionEmptyTile pEmpty) {
		Log.d(TAG, "translateAllTilesHorizontal");
		playSoudEffect(0);
		Canvas c = null;
		lTiles = getAllTilesToMoveH(tile,pEmpty);
		int count = 0;
		float versus = Math.signum(pEmpty.x-tile.posX);
		float step = (float)((GameParameters.TILE_WIDTH + GameParameters.BORDER_WIDTH)/N_STEP_MOVE_TILES);
		while(count<N_STEP_MOVE_TILES){
			count++;
			try {
				c = holder.lockCanvas(null);
				synchronized (holder) {
					for (Tile t : lTiles) {
						t.translateX(step*versus);
					}
					onDraw(c);
				}
			}
			finally {
				if (c != null) {
					holder.unlockCanvasAndPost(c);
				}
			}
		}

		tiles[tile.posX][tile.posY] = null;
		for (Tile t : lTiles) {
			t.posX = t.posX + 1 * (int)versus;
			t.initPosition(this);
			tiles[t.posX][t.posY] = t;
		}
		dumpTiles();
	}
	@SuppressLint("WrongCall")
	private void translateAllTilesVertical(Tile tile, PositionEmptyTile pEmpty) {
		Log.d(TAG, "translateAllTilesVertical");
		playSoudEffect(1);
		Canvas c = null;
		lTiles = getAllTilesToMoveV(tile,pEmpty);
		int count = 0;
		float versus = Math.signum(pEmpty.y-tile.posY);
		float step = (float)((GameParameters.TILE_WIDTH + GameParameters.BORDER_WIDTH)/N_STEP_MOVE_TILES);
		while(count<N_STEP_MOVE_TILES){
			count++;
			try {
				c = holder.lockCanvas(null);
				synchronized (holder) {
					for (Tile t : lTiles) {
						t.translateY(step*versus);
					}
					onDraw(c);
				}
			}
			finally {
				if (c != null) {
					holder.unlockCanvasAndPost(c);
				}
			}
		}

		tiles[tile.posX][tile.posY] = null;
		for (Tile t : lTiles) {
			t.posY = t.posY + 1 * (int)versus;
			t.initPosition(this);
			tiles[t.posX][t.posY] = t;
		}
		dumpTiles();
	}

	private boolean checkSolution() {
		boolean solutionOk = true;
		for (int i = 0; i < GameParameters.NUM_TILES; i++) {
			for (int j = 0; j < GameParameters.NUM_TILES; j++) {
				if(tiles[j][i]!=null && !tiles[j][i].label.equals(j+i*GameParameters.NUM_TILES+1+"")){
					solutionOk = false;
					break;
				}
			}
		}
		return solutionOk;
	}

	public void setBitmapBackground(Bitmap bitmap) {
		if(null==bitmap){
			bitmapBackground = null;
		}
		else{
			float target_w_h = (GameParameters.BORDER_WIDTH + GameParameters.TILE_WIDTH) * GameParameters.NUM_TILES;
			float crop_w = bitmap.getWidth();// h =bitmap.getHeight();
			float scale = target_w_h / crop_w;
			Matrix matrix = new Matrix();
			matrix.postScale(scale, scale);
			Log.d(TAG, "SCALE crop_w:"+crop_w);
			Log.d(TAG, "SCALE target_w_h:"+target_w_h);
			Log.d(TAG, "SCALE MATRIX:"+scale);
			bitmapBackground = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		}

		for (int i = 0; i < GameParameters.NUM_TILES; i++) {
			for (int j = 0; j < GameParameters.NUM_TILES; j++) {
				if(tiles[j][i]!=null){
					tiles[j][i].initBackgroundImage(bitmapBackground);
				}
			}
		}
	}
}