package com.gun;

public class GameParameters {
	public static final int NUM_TILES = 4;
	public static final int BORDER_WIDTH = 3;
	public static int TILE_WIDTH = 80;
	public static float N_STEP_MOVE_TILES = 5;
	public static int NUM_SHAKE_MOVE = 25;						// numero di mosse per mescolare la scacchiera
	public static int VIEW_WIDTH  = 0;
	public static int VIEW_HEIGHT = 0;

//	public static final int ROWS_COLUMNS = 3;					// dimensione della scacchiera
//	public static final int DIMENSIONE_QUADRATO = 2; 			// # di quadrati selezionabili per lato
//	public static final float LATO_QUADRATO = 128.0f;			// lato della casella quadrata 
//	public static final int RADIUS_ROUND = 10;					// raggio per il bordo arrotondato delle caselle
//	public static final float LATO_TEXTURE = 128.0f;
//	public static final float SPAZIO_TRA_QUADRATI = 3.0f;		// spazio tra i quadrati
//	public static final long TIME_TO_ROTATION_MILLISEC = 5L; 	// tempo di rotazione di una selezione
}
