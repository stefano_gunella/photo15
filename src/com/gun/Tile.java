package com.gun;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.Log;
import static com.gun.GameParameters.*;

public class Tile{
	final String TAG = "Tile";
	public int posX=0,posY=0;
	public int startPosX=0,startPosY=0;
	private float currentX=0,currentY=0;
	public Bitmap bitmap = null; // sfondo della casella nel caso ci sia un'immagine di sfondo.

	private RectF tileArea = new RectF();
	public String label = null;
	public GameView gameView = null;
	private Paint paint = new Paint();
	private Paint paintSh = new Paint();

	public Tile(String label,int posX,int posY,GameView parent){
		this.label = label;
		this.posX = posX ;
		this.posY = posY;
		this.startPosX = posX ;
		this.startPosY = posY;
		TILE_WIDTH = (GameParameters.VIEW_WIDTH - GameParameters.NUM_TILES * BORDER_WIDTH - 100)/GameParameters.NUM_TILES;
		TILE_WIDTH = (TILE_WIDTH > 140) ? 140:TILE_WIDTH;
		initPosition(parent);
	}	

	public void initPosition(GameView parent){
		this.gameView = parent;
		currentX = posX * TILE_WIDTH + posX * BORDER_WIDTH + (GameParameters.VIEW_WIDTH/2 - GameParameters.NUM_TILES * (BORDER_WIDTH+TILE_WIDTH)/2);
		currentY = posY * TILE_WIDTH + posY * BORDER_WIDTH + (GameParameters.VIEW_HEIGHT/2 - GameParameters.NUM_TILES * (BORDER_WIDTH+TILE_WIDTH)/2 - 40);
		_updateTileArea(currentX,currentY);
	}

//	public static BitmapShader shader = null;
	public void initBackgroundImage(Bitmap bitmapBackground){
		if(null == bitmapBackground){
			bitmap = null;
		}
		else{
			try{
//				BitmapShader shader = new BitmapShader(bitmapBackground, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
				int bX = startPosX * TILE_WIDTH + posX * BORDER_WIDTH;
				int bY = startPosY * TILE_WIDTH + posY * BORDER_WIDTH;
				bitmap = Bitmap.createBitmap(bitmapBackground, bX, bY, TILE_WIDTH, TILE_WIDTH);
			}
			catch(IllegalArgumentException e){
				bitmap = Bitmap.createBitmap(TILE_WIDTH, TILE_WIDTH, Bitmap.Config.ARGB_8888);
			}
		}
		_initTileBackground();
	}

	private void _updateTileArea(float currentX, float currentY) {
		tileArea.top  = currentY;
		tileArea.left = currentX; 
		tileArea.bottom = currentY + TILE_WIDTH;
		tileArea.right  = currentX + TILE_WIDTH;
	}

	public void drawOn(Canvas canvas){
		canvas.drawBitmap(this.bitmap, tileArea.left, tileArea.top, null);
	}

	private void _initTileBackground(){
		Bitmap tmpBitmap = Bitmap.createBitmap(TILE_WIDTH, TILE_WIDTH, Bitmap.Config.ARGB_8888);
		RectF area = new RectF(BORDER_WIDTH, BORDER_WIDTH, TILE_WIDTH-BORDER_WIDTH,TILE_WIDTH-BORDER_WIDTH);
		Canvas myCanvas = new Canvas(tmpBitmap);
		paintSh.setStrokeWidth(BORDER_WIDTH);
		paintSh.setStyle(Paint.Style.FILL);			
		if(null!=bitmap){
			BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
			paintSh.setAntiAlias(false);
			paintSh.setShader(shader);
			myCanvas.drawRoundRect(area, 12, 12, paintSh);
		}
		else{
			paintSh.setColor(Color.BLACK);
			paintSh.setStyle(Paint.Style.FILL_AND_STROKE);
			myCanvas.drawRoundRect(area, 12, 12, paintSh);
		}	
		/*
		if(null!=bitmap){
			paint.setColor(Color.WHITE);
		}
		else{
			paint.setColor(Color.WHITE);
		}
		*/
		paint.setColor(Color.WHITE);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(BORDER_WIDTH);
		myCanvas.drawRoundRect(area, 12, 12, paint);
		bitmap = tmpBitmap;
		
		paint.setStyle(Paint.Style.FILL);
		paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
		paint.setTextSize((int)(TILE_WIDTH/4));
		myCanvas.drawText(label, BORDER_WIDTH + 20, BORDER_WIDTH + 50 ,paint);
	}

	public void translateX(float tX){
		tileArea.left = currentX + tX;
		updateCurrentPos();
	}

	public void translateY(float tY){
		tileArea.top = currentY + tY;
		updateCurrentPos();
	}

	public void move(float x, float y){
		tileArea.top += y;
		tileArea.left += x;
		updateCurrentPos();
	}

	public void rotate(float angle){
	}

	public boolean isTouched(float x,float y){
		if(tileArea.contains(x, y)) return true;
		return false;
	}

	public void dump(){
		Log.d(TAG, "****** TILE IS ********");
		Log.d(TAG, "label: "+label);
		Log.d(TAG, "currentX: "+currentX);
		Log.d(TAG, "currentY: "+currentY);
		Log.d(TAG, "***********************");
	}

	public void updateCurrentPos() {
		currentY = tileArea.top;
		currentX = tileArea.left;
		_updateTileArea(currentX,currentY);
	}
}